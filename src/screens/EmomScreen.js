import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard
} from "react-native";

import Select from "../components/Select";

import Titulo from "../components/Titulo";

import Time from "../components/Time";

import ProgressBar from "../components/ProgressBar";
import BackgroundProgress from "../components/BackgroundProgress";

import Sound from "react-native-sound";

const alert = require("../../assets/sounds/alert.wav");

class EmomScreen extends Component {
  state = {
    keyboardIsVisible: false,
    alerts: [15],
    countDown: 0,
    time: "1",

    isRunning: false,
    countDownValue: 5,
    count: 0
  };
  componentDidMount() {
    Sound.setCategory("Playback", true);
    this.alert = new Sound(alert);

    this.kbShow = Keyboard.addListener("keyboardDidShow", () => {
      this.setState({ keyboardIsVisible: true });
    });
    this.kbHide = Keyboard.addListener("keyboardDidHide", () => {
      this.setState({ keyboardIsVisible: false });
    });
  }
  componentWillUnmount() {
    this.kbShow.remove();
    this.kbHide.remove();
  }

  playAlert = () => {
    const resto = this.state.count % 60;
    if (this.state.alerts.indexOf(resto) >= 0) {
      this.alert.play();
    }
    if (this.state.countDown === 1) {
      if (resto >= 55 && resto <= 60) {
        this.alert.play();
      }
    }
  };

  play = () => {
    this.setState({ isRunning: true });
    count = () => {
      this.setState({ count: this.state.count + 1 }, () => {
        this.playAlert();
        if (this.state.count === parseInt(this.state.time) * 60) {
          clearInterval(this.countTimer);
        }
      });
    };
    // chegar o countdown
    if (this.state.countDown === 1) {
      this.alert.play();
      this.countDownTimer = setInterval(() => {
        this.alert.play();
        this.setState({ countDownValue: this.state.countDownValue - 1 }, () => {
          if (this.state.countDownValue === 0) {
            clearInterval(this.countDownTimer);
            this.countTimer = setInterval(count, 1000);
          }
        });
      }, 1000);
    } else {
      this.countTimer = setInterval(count, 1000);
    }
    // começar a contar
    // checar se terminou
  };
  render() {
    if (this.state.isRunning) {
      const percMinute = parseInt(((this.state.count % 60) / 60) * 100);
      const percTime = parseInt(
        (this.state.count / 60 / parseInt(this.state.time)) * 100
      );
      return (
        <BackgroundProgress percentage={percMinute}>
          <View style={{ flex: 1, justifyContent: "center" }}>
            <Time time={this.state.count} />
            <ProgressBar percentage={percTime} />
            <Time
              time={parseInt(this.state.time) * 60 - this.state.count}
              type="text2"
              appendedText={" restantes"}
            />
          </View>
        </BackgroundProgress>
      );
    }
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <ScrollView style={styles.container}>
          <Titulo
            titulo={"EMOM"}
            subTitulo={"Every Minute on the Minute"}
            style={{ paddingTop: this.state.keyboardIsVisible ? 20 : 200 }}
          />
          <Image
            source={require("../../assets/cog.png")}
            style={{ alignSelf: "center", width: 40, height: 40 }}
          />
          <Select
            label="Alertas :"
            current={this.state.alerts}
            options={[
              {
                id: 0,
                label: "0s"
              },
              {
                id: 15,
                label: "15s"
              },
              {
                id: 30,
                label: "30s"
              },
              {
                id: 45,
                label: "45s"
              }
            ]}
            onSelect={opt => this.setState({ alerts: opt })}
          />
          <Select
            label="Contagem regressiva :"
            current={this.state.contDown}
            options={[
              {
                id: 1,
                label: "Sim"
              },
              {
                id: 0,
                label: "Não"
              }
            ]}
            onSelect={opt => this.setState({ contDown: opt })}
          />
          <Text
            style={{
              alignSelf: "center",
              color: "white",
              fontFamily: "Ubuntu-Regular",
              fontSize: 24
            }}
          >
            Quantos minutos:
          </Text>
          <TextInput
            style={{
              alignSelf: "center",
              color: "black",
              fontFamily: "Ubuntu-Regular",
              fontSize: 48
            }}
            value={this.state.time}
            keyboardType="numeric"
            onChangeText={text => {
              this.setState({ time: text });
            }}
          />
          <Text
            style={{
              alignSelf: "center",
              color: "white",
              fontFamily: "Ubuntu-Regular",
              fontSize: 24
            }}
          >
            minutos
          </Text>
          <TouchableOpacity
            style={{ alignSelf: "center" }}
            onPress={this.play()}
          >
            <Image
              source={require("../../assets/play.png")}
              style={{ alignSelf: "center", width: 40, height: 40 }}
            />
          </TouchableOpacity>
          <Text style={{ alignSelf: "center", paddingTop: 20 }}>testar</Text>
          <Text>{JSON.stringify(this.state)}</Text>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

EmomScreen.navigationOptions = {
  header: null
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#D6304A"
  }
});

export default EmomScreen;
