import React from "react";
import { View } from "react-native";

const ProgressBar = props => {
  const { color, percentage, height } = props;
  return (
    <View>
      <View
        style={{
          width: percentage ? percentage + "%" : "0",
          backgroundColor: color ? color : "white",
          height: height ? height : 5
        }}
      />
    </View>
  );
};

export default ProgressBar;
