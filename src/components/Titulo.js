import React from "react";
import { View, Text, StyleSheet } from "react-native";

const Titulo = props => {
  return (
    <View style={[styles.container, props.style]}>
      <Text style={styles.textBase}>{props.titulo}</Text>
      <Text style={styles.textDescription}>{props.subTitulo}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { paddingBottom: 20, paddingTop: 20 },
  textBase: {
    fontFamily: "Ubuntu-Bold",
    fontSize: 48,
    color: "#fff",
    textAlign: "center"
  },
  textDescription: {
    fontFamily: "Ubuntu-Regular",
    fontSize: 14,
    color: "white",
    textAlign: "center"
  }
});

export default Titulo;
