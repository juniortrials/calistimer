import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import HomeScreen from "./src/screens/HomeScreen";
import EmomScreen from "./src/screens/EmomScreen";

const AppNavigator = createStackNavigator(
  {
    // Home: HomeScreen,
    Emom: EmomScreen
  },
  { InitialRouteName: "Emom" }
);

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 19,
    textAlign: "center",
    margin: 10,
    fontFamily: "Ubuntu"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
